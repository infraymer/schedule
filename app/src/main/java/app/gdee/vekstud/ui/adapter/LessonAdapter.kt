package app.gdee.vekstud.ui.adapter

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import app.gdee.vekstud.R
import app.gdee.vekstud.entity.Lesson
import app.gdee.vekstud.extension.getTypeColor
import app.gdee.vekstud.extension.inflate
import app.gdee.vekstud.extension.visible
import kotlinx.android.synthetic.main.item_pair.view.*
import org.jetbrains.anko.dip

class LessonAdapter : RecyclerView.Adapter<LessonAdapter.LessonViewHolder>() {

    private var lessons = arrayListOf<Lesson>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        LessonViewHolder(parent.inflate(R.layout.item_pair))

    override fun getItemCount() = lessons.size

    override fun onBindViewHolder(holder: LessonViewHolder, position: Int) =
        holder.onBind(lessons[position])

    fun setData(lessons: ArrayList<Lesson>) {
        this.lessons = lessons
        notifyDataSetChanged()
    }


    class LessonViewHolder(
        private val view: View
    ) : RecyclerView.ViewHolder(view) {

        @SuppressLint("SetTextI18n")
        fun onBind(lesson: Lesson) {
            val res = itemView.context.resources
            val arrBegin = lesson.timeBegin.split(":")
            val arrEnd = lesson.timeEnd.split(":")

            view.timeStartTextView.text = "${arrBegin[0]}:${arrBegin[1]}"
            val rectangle1 = ResourcesCompat.getDrawable(res, R.drawable.rectangle, null)
            rectangle1?.setTint(ResourcesCompat.getColor(res, R.color.colorPrimary, null))
            view.timeStartTextView.background = rectangle1
            view.timeFinishTextView.text = "- ${arrEnd[0]}:${arrEnd[1]}"
            val type = if (lesson.type.name.contains("лаб", true)) "Лаба" else lesson.type.name
            view.typeTextView.text = type
            val rectangle2 = ResourcesCompat.getDrawable(res, R.drawable.rectangle, null)
            rectangle2?.setTint(lesson.getTypeColor())
            view.typeTextView.background = rectangle2
            if (lesson.group?.subgroup != null) {
                val sub = lesson.group.subgroup
                view.subgroupTextView.text = sub.name
                view.subgroupTextView.visible()
                view.typeTextView.background =
                    ResourcesCompat.getDrawable(
                        res, R.drawable.rectangle_left, null)?.apply {
                        setTint(lesson.getTypeColor())
                    }
            }
            view.namePairTextView.text = lesson.subject
            view.placeTextView.text = lesson.location
            lesson.lector?.let {
                view.whoLayout.apply {
                    removeAllViews()
                    addView(view.textView(it.name, R.drawable.ic_person_grey))
                }
            }
            lesson.groups?.let { list ->
                list.forEach {
                    view.whoLayout.apply {
                        removeAllViews()
                        addView(view.textView(it.name, R.drawable.ic_group_grey))
                    }
                }
            }
        }

        private fun View.textView(txt: String, icon: Int) = TextView(context).apply {
            text = txt
            compoundDrawablePadding = dip(8)
            setCompoundDrawablesWithIntrinsicBounds(icon, 0, 0, 0)
            setTextAppearance(context, R.style.WhoStyle)
        }

    }
}