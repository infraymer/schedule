package app.gdee.vekstud.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import app.gdee.vekstud.R
import app.gdee.vekstud.entity.Lesson
import app.gdee.vekstud.entity.Owner
import app.gdee.vekstud.extension.messageAlert
import app.gdee.vekstud.extension.observe
import app.gdee.vekstud.extension.visible
import app.gdee.vekstud.model.MainViewModel
import app.gdee.vekstud.ui.adapter.DateAdapter
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*

class MainActivity : AppCompatActivity() {

    companion object {
        private const val CHOICE_REQUEST = 0
        private const val SEARCH_REQUEST = 1
    }


    private val mainViewModel: MainViewModel by viewModel()
    private lateinit var dateAdapter: DateAdapter

    private var mainPosition = 0

    private var navigationTitle = ""
    private var navigationSubtitle = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(bottomAppBar)
        toolbar.setNavigationIcon(R.drawable.ic_group)
        tabLayout.visible()
        mainViewModel.choiceOwnerLiveData.observe(this) {
            if (it) startActivityForResult(Intent(this, SearchActivity::class.java), CHOICE_REQUEST)
        }
        mainViewModel.stateLiveData.observe(this) { state ->
            //tabLayout.visible(!state.isLoading)
            //progressBar.visible(state.isLoading)
            swipeRefresh.isRefreshing = state.isLoading
            state.error?.getContentIfNotHandled()?.let { msg -> messageAlert(msg) }
            state.data?.let { initAdapter(it) }
            state.title?.let {
                toolbar.title = it
                navigationTitle = it
            }
            /* if (state.title != null) {
                 toolbar.title = state.title
                 navigationTitle = state.title!!
             }*/
        }

        fab.hide()
        fab.setOnClickListener { viewPager.setCurrentItem(mainPosition, true) }
        swipeRefresh.setOnRefreshListener { mainViewModel.onRefresh() }
//        mainViewModel.start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when {
            requestCode == SEARCH_REQUEST && resultCode == Activity.RESULT_OK -> {
                val owner = data?.getParcelableExtra<Owner>(SearchActivity.OWNER_EXTRA)
                startActivity(Intent(this, BrowseActivity::class.java).apply {
                    putExtra(BrowseActivity.OWNER_EXTRA, owner)
                })
            }
            requestCode == CHOICE_REQUEST && resultCode == Activity.RESULT_OK -> {
                val owner = data?.getParcelableExtra<Owner>(SearchActivity.OWNER_EXTRA)
                mainViewModel.onDidChoiceOwner(owner!!)
            }
            requestCode == CHOICE_REQUEST && resultCode == Activity.RESULT_CANCELED -> {
                finish()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.bottomappbar_primary_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                val bottomNavDrawerFragment = BottomNavigationDrawerFragment()
                bottomNavDrawerFragment.show(supportFragmentManager, bottomNavDrawerFragment.tag)
                bottomNavDrawerFragment.onCreated = {
                    it.title = navigationTitle
                    it.subtitle = navigationSubtitle
                }
            }
            R.id.app_bar_search -> startActivityForResult(Intent(this, SearchActivity::class.java), SEARCH_REQUEST)
        }
        return true
    }

    private fun initAdapter(data: Map<String, ArrayList<Lesson>>) {
        dateAdapter = DateAdapter(data, supportFragmentManager)
        viewPager.adapter = dateAdapter
        tabLayout.setupWithViewPager(viewPager)
        mainPosition = dateAdapter.days.indexOf(dateAdapter.currentDate)
        viewPager.currentItem = mainPosition
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                if (position == mainPosition) fab.hide() else fab.show()
            }
        })
    }
}
