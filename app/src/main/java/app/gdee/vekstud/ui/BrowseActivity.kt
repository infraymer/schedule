package app.gdee.vekstud.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.viewpager.widget.ViewPager
import app.gdee.vekstud.R
import app.gdee.vekstud.entity.Lesson
import app.gdee.vekstud.entity.Owner
import app.gdee.vekstud.extension.messageAlert
import app.gdee.vekstud.extension.observe
import app.gdee.vekstud.extension.visible
import app.gdee.vekstud.model.BrowseViewModel
import app.gdee.vekstud.ui.adapter.DateAdapter
import kotlinx.android.synthetic.main.activity_browse.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton
import org.jetbrains.anko.startActivity
import org.koin.android.viewmodel.ext.android.viewModel

class BrowseActivity : AppCompatActivity() {

    companion object {
        const val OWNER_EXTRA = "owner"
    }

    private val viewModel: BrowseViewModel by viewModel()
    private var mainPosition = 0
    private lateinit var dateAdapter: DateAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_browse)
        val owner = intent.getParcelableExtra<Owner>(OWNER_EXTRA)
        toolbar.title = owner.name
        toolbar.inflateMenu(R.menu.bottomappbar_primary_menu)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        toolbar.setOnMenuItemClickListener {
            startActivity<SearchActivity>()
            finish()
            true
        }
        fab.hide()
        fab.setOnClickListener { viewPager.setCurrentItem(mainPosition, true) }
        viewModel.progressLiveData.observe(this, Observer {
            tabLayout.visible(!it)
            progressBar.visible(it)
        })
        viewModel.errorLiveData.observe(this) { event ->
            event.getContentIfNotHandled()?.let { msg ->
                alert(msg) {
                    okButton {
                        it.dismiss()
                        finish()
                    }
                    show()
                }
            }
        }
        viewModel.scheduleLiveData.observe(this) { initAdapter(it) }
        viewModel.getSchedule(owner)
    }

    private fun initAdapter(data: Map<String, ArrayList<Lesson>>) {
        dateAdapter = DateAdapter(data, supportFragmentManager)
        viewPager.adapter = dateAdapter
        tabLayout.setupWithViewPager(viewPager)
        mainPosition = dateAdapter.days.indexOf(dateAdapter.currentDate)
        viewPager.currentItem = mainPosition
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                if (position == mainPosition) fab.hide() else fab.show()
            }
        })
    }
}