package app.gdee.vekstud.ui

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import app.gdee.vekstud.R
import app.gdee.vekstud.model.MainViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.internal.NavigationMenuView
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.fragment_bottom_nav_drawer.*
import org.koin.android.viewmodel.ext.android.sharedViewModel

class BottomNavigationDrawerFragment : BottomSheetDialogFragment() {

    val viewModel: MainViewModel by sharedViewModel()

    var title: String
        get() = /*titleTextView.text.toString()*/""
        set(value) {
            /*titleTextView.text = value*/
        }
    var subtitle: String
        get() = /*subtitleTextView.text.toString()*/""
        set(value) {
            //subtitleTextView.visible(value.isNotEmpty())
            /*subtitleTextView.text = value*/
        }

    var onCreated: (navigation: BottomNavigationDrawerFragment) -> Unit = {}

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_bottom_nav_drawer, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        navigationView.setNavigationItemSelectedListener { menuItem ->
            // Bottom Navigation Drawer menu item clicks
            when (menuItem.itemId) {
                R.id.nav1 -> viewModel.onChoiceGroup()
            }
            // Add code here to update the UI based on the item selected
            // For example, swap UI fragments here
            this.dismiss()
            true
        }

//        closeButton.setOnClickListener {
//            this.dismiss()
//        }

        disableNavigationViewScrollbars(navigationView)
        onCreated(this)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog

        dialog.setOnShowListener { dialog ->
            val d = dialog as BottomSheetDialog

            val bottomSheet = d.findViewById<View>(R.id.design_bottom_sheet) as FrameLayout?
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet!!)
            bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
                override fun onSlide(bottomSheet: View, slideOffset: Float) {
//                    closeButton.visible(slideOffset > 0.5)
                }

                @SuppressLint("SwitchIntDef")
                override fun onStateChanged(bottomSheet: View, newState: Int) {
                    when (newState) {
                        BottomSheetBehavior.STATE_HIDDEN -> dismiss()
//                        else -> closeButton.visible(false)
                    }
                }
            })
        }

        return dialog
    }

    private fun disableNavigationViewScrollbars(navigationView: NavigationView?) {
        val navigationMenuView = navigationView?.getChildAt(0) as NavigationMenuView
        navigationMenuView.isVerticalScrollBarEnabled = false
    }
}