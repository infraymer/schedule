package app.gdee.vekstud.ui.adapter

import android.annotation.SuppressLint
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import app.gdee.vekstud.entity.Lesson
import app.gdee.vekstud.ui.DateFragment
import java.text.SimpleDateFormat
import java.util.*

class DateAdapter(
        private var dates: Map<String, ArrayList<Lesson>>,
        fragmentManager: FragmentManager
) : FragmentStatePagerAdapter(fragmentManager) {

    var days = arrayListOf<Date>()
    var currentDate: Date

    private val calendar = GregorianCalendar.getInstance()
    @SuppressLint("SimpleDateFormat")
    private val dateFormat = SimpleDateFormat("yyyy-MM-dd")

    init {
        currentDate = calendar.time
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
        for (i in 0..12) {
            days.add(calendar.time)
            calendar.add(Calendar.DAY_OF_MONTH, 1)
            if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) calendar.add(Calendar.DAY_OF_MONTH, 1)
        }
    }

    override fun getItem(position: Int) =
            DateFragment.newInstance(dates[dateFormat.format(days[position])] ?: arrayListOf())

    override fun getCount() = 12

    @SuppressLint("SimpleDateFormat")
    override fun getPageTitle(position: Int) = SimpleDateFormat("EE, dd.MM").format(days[position])!!
}