package app.gdee.vekstud.ui.adapter

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import app.gdee.vekstud.R
import app.gdee.vekstud.entity.Owner
import app.gdee.vekstud.extension.inflate

class ListAdapter(
    private val onClick: (owner: Owner) -> Unit = {}
) : RecyclerView.Adapter<ListAdapter.ItemViewHolder>() {

    private var data = arrayListOf<Owner>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ItemViewHolder(parent.inflate(R.layout.item_text))

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) =
        holder.onBind(data[position], onClick)

    fun setData(data: ArrayList<Owner>) {
        this.data = data
        notifyDataSetChanged()
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val text: TextView = view.findViewById(R.id.text)

        fun onBind(item: Owner, onClick: (owner: Owner) -> Unit) {
            text.text = item.name
            text.setOnClickListener { onClick(item) }
        }
    }
}