package app.gdee.vekstud.ui

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.LinearLayoutManager
import app.gdee.vekstud.R
import app.gdee.vekstud.entity.Lesson
import app.gdee.vekstud.ui.adapter.LessonAdapter
import kotlinx.android.synthetic.main.fragment_date.*

class DateFragment : BaseFragment() {

    override val layoutRes = R.layout.fragment_date

    companion object {
        private const val LESSONS = "lessons"
        fun newInstance(lessons: ArrayList<Lesson>) = DateFragment().apply {
            arguments = bundleOf(Pair(LESSONS, lessons))
        }
    }

    private val adapter by lazy { LessonAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter.setData(arguments?.getParcelableArrayList(LESSONS) ?: arrayListOf())
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = this@DateFragment.adapter
        }
    }
}