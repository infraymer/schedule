package app.gdee.vekstud.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import app.gdee.vekstud.R
import app.gdee.vekstud.entity.Owner
import app.gdee.vekstud.extension.debounce
import app.gdee.vekstud.extension.observe
import app.gdee.vekstud.extension.onTextChanged
import app.gdee.vekstud.extension.visible
import app.gdee.vekstud.model.SearchViewModel
import app.gdee.vekstud.ui.adapter.ListAdapter
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.coroutines.experimental.GlobalScope
import kotlinx.coroutines.experimental.channels.consumeEach
import kotlinx.coroutines.experimental.launch
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.concurrent.TimeUnit

class SearchActivity : AppCompatActivity() {

    companion object {
        const val OWNER_EXTRA = "owner"
    }

    private val viewModel: SearchViewModel by viewModel()
    private val adapter: ListAdapter by lazy { ListAdapter(onItemClicked) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        backButton.setOnClickListener { onBackPressed() }
        clearButton.setOnClickListener { inputEditText.text.clear() }
        inputEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                clearButton.visible(s.toString() != "")
            }
        })
        GlobalScope.launch {
            inputEditText.onTextChanged()
                .debounce(300, TimeUnit.MILLISECONDS)
                .consumeEach { viewModel.search(it) }
        }
        recyclerView.apply {
            layoutManager = LinearLayoutManager(this@SearchActivity)
            adapter = this@SearchActivity.adapter
        }
        viewModel.ownersLiveData.observe(this) { adapter.setData(it) }
        viewModel.progressLiveData.observe(this) { progressBar.visible(it, false) }
        viewModel.getOwners()
    }

    private val onItemClicked = { owner: Owner ->
        setResult(Activity.RESULT_OK, Intent().apply { putExtra(OWNER_EXTRA, owner) })
        finish()
    }
}
