package app.gdee.vekstud

import android.app.Application
import app.gdee.vekstud.system.appModule
import org.koin.android.ext.android.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(appModule(this.applicationContext)))
    }
}