package app.gdee.vekstud.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.gdee.vekstud.entity.Owner
import app.gdee.vekstud.entity.ScheduleState
import app.gdee.vekstud.system.Event
import app.gdee.vekstud.system.Settings
import kotlinx.coroutines.experimental.Dispatchers
import kotlinx.coroutines.experimental.GlobalScope
import kotlinx.coroutines.experimental.android.Main
import kotlinx.coroutines.experimental.launch

class MainViewModel(
        private val repository: ScheduleRepository,
        private val settings: Settings
) : ViewModel() {

    val stateLiveData = MutableLiveData<ScheduleState>()
    val choiceOwnerLiveData = MutableLiveData<Boolean>()

    private val state = ScheduleState(title = settings.owner?.name)

    init {
        if (settings.owner == null) {
            choiceOwnerLiveData.value = true
        } else {
            stateLiveData.value = state
            getSchedule(settings.owner!!)
        }
    }

    private fun getSchedule(owner: Owner) = GlobalScope.launch(Dispatchers.Main) {
        try {
            stateLiveData.value = state.apply { isLoading = true }
            val data = repository.getSchedule(owner)
            stateLiveData.value = state.apply {
                this.data = data
                this.title = owner.name
            }
        } catch (e: Exception) {
            e.printStackTrace()
            stateLiveData.value = state.apply {
                error = Event(e.message.toString())
            }
        } finally {
            stateLiveData.value = state.apply { isLoading = false }
        }
    }

    fun onDidChoiceOwner(owner: Owner) {
        settings.owner = owner
        stateLiveData.value = state.apply {
            title = owner.name
            data = null
        }
        getSchedule(owner)
    }

    fun onChoiceGroup() {
        choiceOwnerLiveData.value = true
    }

    fun onRefresh() {
        settings.owner?.let { getSchedule(it) }
    }
}