package app.gdee.vekstud.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.gdee.vekstud.entity.Lesson
import app.gdee.vekstud.entity.Owner
import app.gdee.vekstud.system.Event
import kotlinx.coroutines.experimental.Dispatchers
import kotlinx.coroutines.experimental.GlobalScope
import kotlinx.coroutines.experimental.android.Main
import kotlinx.coroutines.experimental.launch

class BrowseViewModel(
        private val repository: ScheduleRepository
) : ViewModel() {

    val scheduleLiveData = MutableLiveData<Map<String, ArrayList<Lesson>>>()
    val progressLiveData = MutableLiveData<Boolean>()
    val errorLiveData = MutableLiveData<Event<String>>()

    fun getSchedule(owner: Owner) = GlobalScope.launch(Dispatchers.Main) {
        try {
            progressLiveData.value = true
            val data = repository.getSchedule(owner)
            scheduleLiveData.value = data
        } catch (e: Exception) {
            e.printStackTrace()
            errorLiveData.value = Event(e.message.toString())
        } finally {
            progressLiveData.value = false
        }
    }
}