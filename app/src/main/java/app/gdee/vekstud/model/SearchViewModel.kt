package app.gdee.vekstud.model

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.gdee.vekstud.entity.Owner
import kotlinx.coroutines.experimental.Dispatchers
import kotlinx.coroutines.experimental.GlobalScope
import kotlinx.coroutines.experimental.android.Main
import kotlinx.coroutines.experimental.launch

class SearchViewModel(
    private val repository: ScheduleRepository
) : ViewModel() {

    val progressLiveData = MutableLiveData<Boolean>()
    val ownersLiveData = MutableLiveData<ArrayList<Owner>>()

    private var owners = arrayListOf<Owner>()

    fun getOwners() = GlobalScope.launch(Dispatchers.Main) {
        try {
            progressLiveData.value = true
            owners = repository.getOwners()
            ownersLiveData.value = owners
        } catch (e: Exception) {
            Log.d(javaClass.simpleName, e.message)
        } finally {
            progressLiveData.value = false
        }
    }

    fun search(text: String) {
        ownersLiveData.postValue(
            owners.filter { it.name.contains(text, true) } as ArrayList<Owner>
        )
    }
}