package app.gdee.vekstud.model

import android.annotation.SuppressLint
import app.gdee.vekstud.entity.Lesson
import app.gdee.vekstud.entity.Owner
import app.gdee.vekstud.extension.obj
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import khttp.get
import kotlinx.coroutines.experimental.Dispatchers
import kotlinx.coroutines.experimental.GlobalScope
import kotlinx.coroutines.experimental.IO
import kotlinx.coroutines.experimental.async
import java.text.SimpleDateFormat
import java.util.*

class ScheduleRepository {

    private val gson = Gson()
    private var owners: ArrayList<Owner>? = null

    companion object {
        private const val URL = "https://schedule.vekclub.com/api/v1/"
        private const val TIMEOUT = 5.0
    }

    suspend fun getOwners(): ArrayList<Owner> {
        if (owners != null) return owners!!
        val groupsJson = GlobalScope.async(Dispatchers.IO) { get(URL + "handbook/groups-by-institution?institutionId=4", timeout = TIMEOUT).obj["data"].asJsonArray }.await()
        val lectorsJson = GlobalScope.async(Dispatchers.IO) { get(URL + "handbook/lecturers-in-institution?institutionId=4", timeout = TIMEOUT).obj["data"].asJsonArray }.await()
        val groups = gson.fromJson<ArrayList<Owner>>(groupsJson, object : TypeToken<ArrayList<Owner>>() {}.type)
        val lectors = gson.fromJson<ArrayList<Owner>>(lectorsJson, object : TypeToken<ArrayList<Owner>>() {}.type)
        groups.forEach { it.isGroup = true }
        lectors.forEach { it.isGroup = false }
        groups.addAll(lectors)
        owners = groups
        return owners!!
    }

    @SuppressLint("SimpleDateFormat")
    suspend fun getSchedule(owner: Owner): Map<String, ArrayList<Lesson>> {
        val df = SimpleDateFormat("yyyy-MM-dd")
        val calendar = GregorianCalendar.getInstance()
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
        val dateBegin = df.format(calendar.time)
        calendar.add(Calendar.DAY_OF_MONTH, 14)
        val dateEnd = df.format(calendar.time)
        val date = "&dateBegin=$dateBegin&dateEnd=$dateEnd"
        val link = if (owner.isGroup) "group?groupId=${owner.id}" else "lector?lectorId=${owner.id}"
        val lessons = GlobalScope.async {
            val response = get(URL + "schedule/$link$date", timeout = TIMEOUT)
            response.obj["data"].asJsonObject
        }.await()
        return gson.fromJson(lessons, object : TypeToken<Map<String, ArrayList<Lesson>>>() {}.type)
    }

}