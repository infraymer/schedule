package app.gdee.vekstud.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Entity(
    val id: Int,
    val name: String
) : Parcelable