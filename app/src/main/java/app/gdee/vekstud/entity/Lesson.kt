package app.gdee.vekstud.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Lesson(
    @SerializedName("date") val date: String,
    @SerializedName("time_begin") val timeBegin: String,
    @SerializedName("time_end") val timeEnd: String,
    @SerializedName("subject_name") val subject: String,
    @SerializedName("lesson_type") val type: Entity,
    @SerializedName("lector") val lector: Lector?,
    @SerializedName("group") val group: Group?,
    @SerializedName("groups") val groups: List<Group>?,
    @SerializedName("location") val location: String
) : Parcelable