package app.gdee.vekstud.entity

import app.gdee.vekstud.system.Event

data class ScheduleState(
    var isLoading: Boolean = false,
    var error: Event<String>? = null,
    var data: Map<String, ArrayList<Lesson>>? = null,
    var title: String? = null
)