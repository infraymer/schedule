package app.gdee.vekstud.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Lector(
    @SerializedName("uuid") val uuid: String,
    @SerializedName("lector_id") val id: Int,
    @SerializedName("lector_name") val name: String
) : Parcelable