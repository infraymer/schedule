package app.gdee.vekstud.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Group(
    @SerializedName("uuid") val uuid: String,
    @SerializedName("group_id") val id: Int,
    @SerializedName("group_name") val name: String,
    @SerializedName("subgroup") val subgroup: Subgroup? = null
) : Parcelable