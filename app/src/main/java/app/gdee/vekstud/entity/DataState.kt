package app.gdee.vekstud.entity

data class DataState(
    var isLoading: Boolean = false,
    var error: String? = null,
    val data: Any? = null
)