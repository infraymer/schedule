package app.gdee.vekstud.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Owner(
    val id: Int,
    val name: String,
    var isGroup: Boolean = true
) : Parcelable