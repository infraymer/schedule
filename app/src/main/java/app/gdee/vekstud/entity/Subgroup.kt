package app.gdee.vekstud.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Subgroup(
    @SerializedName("subgroup_id") val id: Int,
    @SerializedName("subgroup_name") val name: String
) : Parcelable