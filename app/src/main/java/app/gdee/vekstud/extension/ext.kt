package app.gdee.vekstud.extension

import android.content.Context
import android.graphics.Color
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.annotation.LayoutRes
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import app.gdee.vekstud.entity.Lesson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import khttp.responses.Response
import kotlinx.coroutines.experimental.GlobalScope
import kotlinx.coroutines.experimental.channels.Channel
import kotlinx.coroutines.experimental.channels.ReceiveChannel
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.selects.whileSelect
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton
import java.net.SocketTimeoutException
import java.util.concurrent.TimeUnit


fun View.visible(show: Boolean = true, gone: Boolean = true) {
    visibility = if (show) View.VISIBLE else if (gone) View.GONE else View.INVISIBLE
}

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

val Response.obj: JsonObject
    get() = JsonParser().parse(text).asJsonObject

val Response.array: JsonArray
    get() = JsonParser().parse(text).asJsonArray

fun Context.messageAlert(msg: String) = alert(msg) {
    okButton { it.dismiss() }
    show()
}

fun <T> LiveData<T>.observe(owner: LifecycleOwner, block: (T) -> Unit) {
    observe(owner, Observer<T> { block(it) })
}

fun EditText.onTextChanged(): ReceiveChannel<String> =
        Channel<String>(capacity = Channel.UNLIMITED).also { channel ->
            addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(editable: Editable?) {
                    editable?.toString().orEmpty().let(channel::offer)
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

            })
        }

fun <T> ReceiveChannel<T>.debounce(time: Long, unit: TimeUnit = TimeUnit.MILLISECONDS): ReceiveChannel<T> =
        Channel<T>(capacity = Channel.CONFLATED).also { channel ->
            GlobalScope.launch {
                var value = receive()
                whileSelect {
                    onTimeout(unit.toMillis(time), {
                        channel.offer(value)
                        value = receive()
                        true
                    })
                    onReceive {
                        value = it
                        true
                    }
                }
            }
        }

fun Lesson.getTypeColor() = when (type.id) {
    1 -> Color.parseColor("#4e95f4")
    2 -> Color.parseColor("#2da9bf")
    3 -> Color.parseColor("#77cb1c")
    4 -> Color.parseColor("#815bde")
    5 -> Color.parseColor("#ee4364")
    6 -> Color.parseColor("#fb9500")
    else -> Color.parseColor("#915437")
}

fun Exception.messageHandler() = when (this) {
    is IllegalStateException -> "Расписание не найдено"
    is SocketTimeoutException -> "Время ответа от сервера истекло. Возможно проблемы с Интернет соединением."
    else -> "Неизвестная ошибка"
}