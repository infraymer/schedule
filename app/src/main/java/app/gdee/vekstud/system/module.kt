package app.gdee.vekstud.system

import android.content.Context
import app.gdee.vekstud.model.BrowseViewModel
import app.gdee.vekstud.model.MainViewModel
import app.gdee.vekstud.model.ScheduleRepository
import app.gdee.vekstud.model.SearchViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

fun appModule(context: Context) = module {

    single { Settings(context) }
    single { ScheduleRepository() }

    viewModel { MainViewModel(get(), get()) }
    viewModel { BrowseViewModel(get()) }
    viewModel { SearchViewModel(get()) }
}