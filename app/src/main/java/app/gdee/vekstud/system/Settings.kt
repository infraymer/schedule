package app.gdee.vekstud.system

import android.content.Context
import androidx.core.content.edit
import app.gdee.vekstud.entity.Owner
import com.google.gson.Gson
import org.jetbrains.anko.defaultSharedPreferences

class Settings(
    context: Context
) {

    companion object {
        private const val OWNER = "owner"
    }

    private val pref = context.defaultSharedPreferences
    private val gson = Gson()

    var owner: Owner?
        get() = gson.fromJson(pref.getString(OWNER, null), Owner::class.java)
        set(value) {
            pref.edit { putString(OWNER, gson.toJson(value)) }
        }
}